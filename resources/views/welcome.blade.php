<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet"><title>ea-client</title><link href="/js/about.75e8ea9a.js" rel="prefetch"><link href="/js/chunk-ae5be27e.41faa42b.js" rel="prefetch"><link href="/css/chunk-vendors.f3509585.css" rel="preload" as="style"><link href="/js/app.13d9cc2e.js" rel="preload" as="script"><link href="/js/chunk-vendors.28de02a7.js" rel="preload" as="script">
        <link href="/css/chunk-vendors.f3509585.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
        <link href="/js/about.75e8ea9a.js" rel="prefetch">
        <link href="/js/chunk-ae5be27e.41faa42b.js" rel="prefetch">
        <link href="/css/chunk-vendors.f3509585.css" rel="preload" as="style">
        <link href="/js/app.13d9cc2e.js" rel="preload" as="script">
        <link href="/js/chunk-vendors.28de02a7.js" rel="preload" as="script">
        <link href="/css/chunk-vendors.f3509585.css" rel="stylesheet">
    </head>
    <body>
        <noscript>
            <strong>We're sorry but ea-client doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
        </noscript>
        <div id="app"></div>
        <script>
            window.hostUrl = "{{ url('/') }}"
        </script>
        <script src="/js/chunk-vendors.28de02a7.js"></script>
        <script src="/js/app.13d9cc2e.js"></script>
    </body>
</html>
