<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Request as RequestModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CommentController extends Controller
{
    /**
     * Create a new instance of this controller
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Register a new request
     *
     * @return \App\Models\Comment
     */
    public function store()
    {
        $reqData = request(['request_id', 'content']);
        $rules = array(
            'request_id' => 'required|numeric',
            'content' => 'required|string'
        );

        $validator = Validator::make($reqData, $rules);

        $errorMessage = null;

        if ($validator->fails())
            $errorMessage = "Input parameters are invalid or missing";
        else if (RequestModel::find($reqData['request_id']) == null)
            $errorMessage = "Request with this `request_id` is not exist";
        if ($errorMessage != null)
            return response()->json($errorMessage, 400);

        $comment = Comment::create($reqData);
        $comment->save();
        return $comment;
    }

    /**
     * Get comments for a request
     *
     * @return Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index()
    {
        $requestId = request('request_id');
        $rules = array(
            'request_id' => 'required|numeric',
        );

        $validator = Validator::make(array("request_id", $requestId), $rules);

        if ($validator->fails()) {
            $req = RequestModel::find($requestId);
            if ($req == null)
                $errorMessage = "Request with this `request_id` is not exist";
        }
        if (isset($errorMessage))
            return response()->json($errorMessage, 400);

        return $req->comments()
                   ->orderBy('updated_at', 'DESC')
                   ->paginate(10);
    }
}
