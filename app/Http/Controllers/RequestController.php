<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Models\Request as RequestModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RequestController extends Controller
{
    /**
     * Create a new instance of this controller
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except('store');
    }

    /**
     * Register a new request
     *
     * @return \App\Models\Request
     */
    public function store()
    {
        $requestData = request(['heading', 'content', 'photo', 'mime_type']);

        $rules = array(
            'heading' => 'required|string',
            'content' => 'required|string',
            'photo' => 'string|nullable',
            'mime_type' => 'required_with:photo|string|nullable'
        );

        $validator = Validator::make($requestData, $rules);

        if ($validator->fails())
            return response()->json(['messages' => $validator->messages()], 400);

        if (isset($requestData['photo'])) {
            $photoBase64String = $requestData['photo'];
            $photoBin = base64_decode($requestData['photo']);
            $photoName = Str::random(40);
            $photoPath = 'photos' . DIRECTORY_SEPARATOR . $photoName;
            Storage::put($photoPath, $photoBin);
            $requestData['photo'] = $photoPath;
        }

        $requestModel = \App\Models\Request::create($requestData);
        $requestModel->save();

        if (isset($photoBase64String))
            $requestModel->photo = $photoBase64String;
        return $requestModel;
    }

    /**
     * Get paginated requests
     *
     * @return Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index()
    {
        $status = request('status');

        $query = DB::table("requests");

        if (in_array($status, ["new", "handled"]))
            $query = $query->where('is_new', '=', $status == 'new');

        $reqs = $query->orderBy('updated_at', 'DESC')
                      ->paginate(10);

        foreach ($reqs as $req) {
            if (!isset($req->photo))
                continue;
            $photoBin = Storage::get($req->photo);
            $req->photo = base64_encode($photoBin);
        }
        return $reqs;
    }

    /**
     * Mark request as handled
     *
     * @return \App\Models\Request
     */
    public function update($id)
    {
        return RequestModel::where('id', '=', $id)
            ->update(["is_new" => false]);
    }
}
