<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    use HasFactory;

    protected $fillable = array("heading",
        "content",
        "mime_type",
        "photo"
    );

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
