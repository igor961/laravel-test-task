This is my test task for the Laravel developer position. 

The app recieves requests from any users. Special user - manager - can watch all the requests, handle it, write comments.

To run this app you need Docker to be installed. It uses `sail` instrument for development. For detailed instruction refer https://laravel.com/docs/8.x/sail#starting-and-stopping-sail

For running command `sail` you can set an alias or run it directly: `./vendor/bin/sail up` - for example

1) ./vendor/bin/sail up
2) ./vendor/bin/sail artisan migrate
3) ./vendor/bin/sail artisan db:seed
4) Open a browser on http://localhost
