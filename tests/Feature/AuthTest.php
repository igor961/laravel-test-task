<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A login test.
     *
     * @return void
     */
    public function testLoginTest()
    {
        $response = $this->post('http://localhost/api/auth/login', array(
            "email" => "hello@test.task",
            "password" => "password"
        ));

        $response->assertStatus(200);
        $this->assertTrue(strlen($response['access_token']) > 0);
    }
}
